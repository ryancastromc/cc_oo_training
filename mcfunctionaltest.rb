require "sourcify"
require "mastercontrol-test-suite/helpers/configure_helpers_db"
require "mastercontrol-test-suite/helpers/tools/file_helper"
require "mastercontrol-test-suite/helpers/tools/track_helper"
$stdout.sync = true
#Redirect STDERR to STOUDOUT so that run.rb gets errors and stack traces, rather than just text puts to it.
$stderr = $stdout
$workflow_times = []

class MCFunctionalTest
  include FileHelper
  include TrackHelper

  attr_writer :failed

  def gather_methods_and_times method_id
    current_method_info = {:name => "#{method_id.to_s}", :start => Time.new.to_s}
    $workflow_times << current_method_info
  end

  def run_api_and_retry &block
    tries ||= 1
    yield(block)
  rescue RestClient::Conflict
  rescue RestClient::Unauthorized => e
    (tries += 1) > 3 ? raise(e) : (sleep(rand(0.5..1.0)) && retry)
  end

  def create_test_user
    user = env['test_file_name']
    role = "FT_ADMIN"
    MCAPI.new
  rescue RestClient::Unauthorized, RestClient::Forbidden
    p "No account for #{user}. Making one."
    connection = MCAPI.new(env['sysadmin'], env['sysadmin_password'])
    run_api_and_retry do
      MCAPIs.create_user(env['test_file_name'], roles: [role], connection: connection)
    end
  end

  def load_environment
    $workflow_times << {:name => "initializing_test", :start => (Time.new - 1).to_s}

    default_config = MasterControl.default_config_file_path
    personal_config = "#{MasterControlTestSuite.base_path}/my_configuration.json"
    environment_config = ENV['roobee_load_this_config']

    Environment.load(default_config)
    Environment.load(personal_config) if File.exist?(personal_config)
    Environment.load(environment_config) if environment_config
  end

  def setup(has_test_user:true)
    @screenshot_retry_times = 0
    env["test_file_name"] = File.basename($0, ".rb").upcase.gsub(/TEST_/, "")[0...30] unless env['test_file_name']
    unless env['admin_user']
      env["admin_user"] = env['test_file_name']
      env["admin_pass"] = "1"
      env["admin_esig"] =  "2"
    end

    if has_test_user
      create_test_user
      puts ":user>>  Test user: #{env['admin_user']}  <<:"
    end

    @mc = MasterControl.new env['site_url'], env
    @start_time = Time.new

    all_instance_methods = self.class.instance_methods(false) - [:test_this]
    all_instance_methods.select! { |meth| meth.to_s =~ /^test_|^pre_test/ }

    trace = TracePoint.new(:call) do |tp|
      gather_methods_and_times(tp.method_id) if all_instance_methods.include? tp.method_id
    end
    trace.enable
  end

  def teardown
    puts ":time>>  Test run time: #{(Time.new-@start_time).round} seconds  <<:" if @start_time
    return unless @mc
    take_wd_screenshot if failed?
    take_screenshot if failed?
    take_screenshot(js_console: true) if failed? && env['take_js_console_screenshot_on_error']

    if @original_mc_session_displaced_by_clean_up
      @mc.driver.quit
      @original_mc_session_displaced_by_clean_up.quit if env["close_browser_on_exit"]
    else
      @mc.quit if env["close_browser_on_exit"]
    end

  rescue => e
    puts "Note: Test 'teardown' failed. Exception: #{e} - #{e.message}"
  ensure
    $workflow_times << {:name => __method__.to_s, :start => Time.new.to_s}
    workflow_content = times_parser($workflow_times)
    puts ":workflow_times>>#{workflow_content}<<:"
  end

  def take_wd_screenshot mc = nil
    mc = mc || @mc
    return unless Gem.win_platform?
    begin
    path = screenshot_path
    mc.driver.take_screenshot(path)
    puts ":ws>>  Webdriver Screenshot:{#{path}}  <<:"
    rescue => error
      @screenshot_retry_times += 1
      retry if error.class == NoMethodError && @screenshot_retry_times < 3
    end
  end
  def take_screenshot js_console: false
    return unless Gem.win_platform?
    path = screenshot_path
    saved_successfully = take_native_screenshot path, js_console: js_console
    puts ":ns>>  Native Screenshot:{#{path}}  <<:" if saved_successfully
  end


  def warning(message)
    take_screenshot
    puts "\n\n:warning>> WARNING: #{message} <<:\n\n"
  end

  #Returns all object that has inherited Class.
  def self.descendants
    ObjectSpace.each_object(Class).select { |klass| klass < self }
  end

  def failed?
    @failed
  end

  def begin_clean_up
    @original_mc_session_displaced_by_clean_up = @mc
    @mc = @mc.open_second_session

    self.clean_up
  end

  private

  def open_js_console
    @mc.driver._im_a_cheater_webdriver.find_element(tag_name:"body").send_keys :control, :shift, "j"
    sleep 1.5
  end

  def take_native_screenshot path, js_console: false
    require 'win32/screenshot'
    open_js_console if js_console
    take_screenshot_of_desktop path
  end

  def take_screenshot_of_desktop path
    Win32::Screenshot::Take.of(:desktop).write(path)
    true
  rescue
    false
  end

  def screenshot_path
    file_name = File.basename($0)
    guid = rand(36**10).to_s(36)
    "#{env['screenshot_path']}/#{file_name}_#{guid}.png"
  end

end

at_exit do
  MCFunctionalTest.descendants.each do |thing|
    thing = thing.new
    if thing.respond_to? :test_this
      begin
        thing.load_environment
        thing.setup
        thing.test_this
      rescue => e
        thing.failed = true
        raise e
      ensure
        if thing.respond_to? :clean_up
          begin
            thing.begin_clean_up
          rescue => e
            puts ":warning>>  Warning - clean up failed. #{e}  <<:"
          end
        end
        thing.teardown
        puts ":result>>  #{File.basename($0, '.rb')} #{thing.failed? ? 'failed' : 'passed'}.  <<:"
      end
    end
  end
end
