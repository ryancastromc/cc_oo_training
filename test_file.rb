require_relative 'mcfunctionaltest'

class TestLogin < MCFunctionalTest

  def test_this
    test_one
    test_two
    test_three
  end

  def test_one
    random_sleep
  end

  def test_two
    raise
    random_sleep
  rescue
    #Do nothing. just shows exceptions are capable of getting timing data.
  end

  def test_three
    random_sleep
  end

private
  # Doing this only to make it look as much like a normal test as possible.
  def initialize
    setup
    test_this
  ensure
    teardown
  end

  def random_sleep
    value = rand(1..4)
    puts "sleeping for #{value}"
    sleep value
  end
end


TestLogin.new
