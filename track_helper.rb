require 'time'

module TrackHelper
  @@file = "WEBVTT\n"

  def get_time_difference time1, time2
    difference = (Time.parse(time2) - Time.parse(time1)).to_i
    difference
  end

  def format_time(time_in_seconds)
    Time.at(time_in_seconds).utc.strftime("%H:%M:%S")
  end

  def times_parser times
    last_time = 0
    total_time = get_time_difference(times[0][:start], times[times.length-1][:start])

    times.each_with_index do |item, index|
      next_index = index == (times.length - 1) ? times[times.length - 1][:start] : times[index + 1][:start]

      start_time = item[:start]
      end_time = next_index

      time_diff = get_time_difference(start_time, end_time)
      print_time = last_time + time_diff
      if item[:name] != "teardown"
        @@file += "\n#{index + 1}\n"
        @@file += "#{format_time(last_time)}.000 --> #{format_time(print_time)}.000\n"
        @@file += "#{format_name(item[:name])}\n"
        last_time = index == times.length - 2 ? total_time : print_time
      end
    end
    return @@file
  end

  def format_name name
    "#{name.gsub("_", " ").capitalize}"
  end
end
